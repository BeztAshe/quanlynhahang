﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuanLyNhaHang.View_Model
{
    public class HoaDonVM
    {
        public int Id { get; set; }
        public string MaBan { get; set; }
        public decimal TongTien { get; set; }
        public DateTime? GioBatDau { get; set; }
        public DateTime? GioKetThuc { get; set; }
        public ICollection<HoaDonChiTietVM> HoaDonChiTietVM { get; set; }
        public bool isPaid { get; set; }
    }
}