﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuanLyNhaHang.View_Model
{
    public class HoaDonChiTietVM
    {
        public int Id { get; set; }
        public int MonAnId { get; set; }
        public string TenMon { get; set; }
        public decimal DonGia { get; set; }
        public int SL { get; set; }
        public decimal ThanhTien { get; set; }
    }
}