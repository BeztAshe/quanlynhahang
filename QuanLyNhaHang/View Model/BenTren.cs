﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuanLyNhaHang.View_Model
{
    public class BenTren
    {
        public int MonAnId { get; set; }
        public string TenMon { get; set; }
        public decimal DonGia { get; set; }
        public string HinhAnh { get; set; }
    }
}