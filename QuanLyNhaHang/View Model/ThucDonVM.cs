﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuanLyNhaHang.View_Model
{
    public class ThucDonVM
    {
        public int Id { get; set; }
        public DayOfWeek dayOfWeek { get; set; }
        public ICollection<ThucDonChiTietVM> thucDonChiTietVMs { get; set; }
    }
}