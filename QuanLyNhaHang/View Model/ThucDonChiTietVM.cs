﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuanLyNhaHang.View_Model
{
    public class ThucDonChiTietVM
    {
        public int Id { get; set; }
        public int MonAnId { get; set; }
        public string TenMon { get; set; }
        public decimal ThanhTien { get; set; }
        public string HinhAnh { get; set; }
    }
}