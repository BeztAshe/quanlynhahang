﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuanLyNhaHang.View_Model
{
    public class ChinhSuaVM
    {
        public int Id { get; set; }
        public DayOfWeek DayOfWeek { get; set; }
        public ICollection<BenTren> BenTrens { get; set; }
        public ICollection<BenDuoi> BenDuois { get; set; }
    }
}