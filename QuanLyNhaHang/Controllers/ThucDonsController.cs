﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QuanLyNhaHang.Models;
using QuanLyNhaHang.View_Model;

namespace QuanLyNhaHang.Controllers
{
    public class ThucDonsController : Controller
    {

        private QuanLyNhaHangContext db = new QuanLyNhaHangContext();

        // GET: ThucDons
        public async Task<ActionResult> Index()
        {
            return View(await db.ThucDons.ToListAsync());
        }

        // GET: ThucDons/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ThucDon thucDon = await db.ThucDons.FindAsync(id);
            var ChiTiet = thucDon.ThucDonChiTiets.ToList();
            ThucDonVM menu = new ThucDonVM();
            List<ThucDonChiTietVM> lstdetailmenu = new List<ThucDonChiTietVM>();
            foreach(var item in ChiTiet)
            {
                ThucDonChiTietVM menudetail = new ThucDonChiTietVM();
                menudetail.MonAnId = item.MonAnId;
                menudetail.TenMon = item.MonAn.TenMon;
                menudetail.HinhAnh = item.MonAn.HinhAnh;
                lstdetailmenu.Add(menudetail);
            }

            menu.dayOfWeek = thucDon.DayOfWeek;
            menu.Id = thucDon.Id;
            menu.thucDonChiTietVMs = lstdetailmenu;

            if (thucDon == null)
            {
                return HttpNotFound();
            }
            return View(menu);
        }

        // GET: ThucDons/Edit/5
        public async Task<ActionResult> Edit(int? id, int? MonAnId)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ThucDon thucDon = await db.ThucDons.FindAsync(id);
            var chiTiet = thucDon.ThucDonChiTiets.ToList();

            ChinhSuaVM fix = new ChinhSuaVM();
            List<BenTren> upperlst = new List<BenTren>();
            List<BenDuoi> lowerlst = new List<BenDuoi>();
            var dishes = db.MonAns.ToList();
            for (int i = 0; i < dishes.Count(); i++)
            {
                var dish = lowerlst.Where(x => x.MonAnId == dishes[i].Id).FirstOrDefault();
                if(dish == null)
                {
                    BenTren upper = new BenTren();
                    upper.MonAnId = dishes[i].Id;
                    upper.TenMon = dishes[i].TenMon;
                    upper.DonGia = dishes[i].DonGia;
                    upper.HinhAnh = dishes[i].HinhAnh;
                    upperlst.Add(upper);
                }
            }

            for(int j = 0; j < dishes.Count(); j++)
            {
                var dish1 = upperlst.Where(x => x.MonAnId == dishes[j].Id).FirstOrDefault();
                if(dish1 == null)
                {
                    BenDuoi lower = new BenDuoi();
                    lower.MonAnId = dishes
                }
            }

            return View(fix);
        }

        // POST: ThucDons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,DayOfWeek")] ThucDon thucDon)
        {
            if (ModelState.IsValid)
            {
                db.Entry(thucDon).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(thucDon);
        }

        // GET: ThucDons/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ThucDon thucDon = await db.ThucDons.FindAsync(id);
            if (thucDon == null)
            {
                return HttpNotFound();
            }
            return View(thucDon);
        }

        // POST: ThucDons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ThucDon thucDon = await db.ThucDons.FindAsync(id);
            db.ThucDons.Remove(thucDon);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
