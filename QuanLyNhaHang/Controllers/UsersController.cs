﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QuanLyNhaHang.Models;
using DotNetOpenAuth.AspNet.Clients;
using Facebook;
using System.Configuration;
using FacebookClient = Facebook.FacebookClient;

namespace QuanLyNhaHang.Controllers
{
    public class UsersController : Controller
    {
        private QuanLyNhaHangContext db = new QuanLyNhaHangContext();

        private Uri RedirectUri
        {
            get
            {
                var uriBuilder = new UriBuilder(Request.Url);
                uriBuilder.Query = null;
                uriBuilder.Fragment = null;
                uriBuilder.Path = Url.Action("FacebookCallback");
                return uriBuilder.Uri;
            }
        }
        
        //GET: Users/Login
        [HttpGet]
        [Route("Users/LogIn")]
        public ActionResult Login()
        {
            HttpCookie cookie = Request.Cookies["user_id"];
            if(cookie != null)
            {
                var userid = Guid.Parse(cookie.Value);
                var userloggedin = db.Users.SingleOrDefault(x => x.Id == userid);
                Session["username"] = userloggedin.Name;
            }
            if (Session["username"] != null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View();
            }
        }

        //POST: Users/Login
        [HttpPost]
        public ActionResult LogIn(User user)
        {
            var userloggedin = db.Users.SingleOrDefault(x => x.Email == user.Email && x.Password == user.Password);

            if (userloggedin != null)
            {
                //Tạo cookie
                HttpCookie usercookie = new HttpCookie("user_id", userloggedin.Id.ToString());
                //ngày hết hạn
                usercookie.Expires.AddDays(60);
                //Lưu data vào cookie
                HttpContext.Response.SetCookie(usercookie);

                ViewBag.message = "loggedin";
                ViewBag.triedOnce = "yes";
                Session["username"] = userloggedin.Email;
                Session["name"] = userloggedin.Name;
                Session["id"] = userloggedin.Id;

                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.triedOnce = "yes";
                return View();
            }
        }

        public ActionResult LogInFacebook()
        {
            var fb = new FacebookClient();
            var loginUrl = fb.GetLoginUrl(new
            {
                client_id = ConfigurationManager.AppSettings["FbAppId"],
                client_secret = ConfigurationManager.AppSettings["FbAppSecret"],
                redirect_uri = RedirectUri.AbsoluteUri,
                response_type = "code",
                scope = "email",    
            });

            return Redirect(loginUrl.AbsoluteUri);
        }

        public ActionResult FacebookCallback(string code)
        {
            var fb = new FacebookClient();
            dynamic result = fb.Post("oauth/access_token", new
            {
                client_id = ConfigurationManager.AppSettings["FbAppId"],
                client_secret = ConfigurationManager.AppSettings["FbAppSecret"],
                redirect_uri = RedirectUri.AbsoluteUri,
                code = code
            });

            var accesstoken = result.access_token;
            if(!String.IsNullOrEmpty(accesstoken))
            {
                fb.AccessToken = accesstoken;
                dynamic me = fb.Get("me?fields=first_name, middle_name, last_name,email");
                string Email = me.email;
                string FirstName = me.first_name;
                string MiddleName = me.middle_name;
                string LastName = me.last_name;

                var user = new User();
                user.Email = Email;
                user.Name = FirstName + " " + MiddleName + " " + LastName;
                user.Role = Role.Staff;

                var usercheck = db.Users.Where(x => x.Email.Contains(user.Email)).FirstOrDefault();
                if(usercheck != null)
                {
                    HttpCookie usercookie = new HttpCookie("user_id", usercheck.Id.ToString());
                    //ngày hết hạn
                    usercookie.Expires.AddDays(60);
                    //Lưu data vào cookie
                    HttpContext.Response.SetCookie(usercookie);

                    ViewBag.message = "loggedin";
                    ViewBag.triedOnce = "yes";
                    Session["username"] = usercheck.Email;
                    Session["name"] = usercheck.Name;
                    Session["id"] = usercheck.Id;

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    db.Users.Add(user);
                    db.SaveChanges();
                    HttpCookie usercookie = new HttpCookie("user_id", usercheck.Id.ToString());
                    //ngày hết hạn
                    usercookie.Expires.AddDays(60);
                    //Lưu data vào cookie
                    HttpContext.Response.SetCookie(usercookie);

                    ViewBag.message = "loggedin";
                    ViewBag.triedOnce = "yes";
                    Session["username"] = usercheck.Email;
                    Session["name"] = usercheck.Name;
                    Session["id"] = usercheck.Id;

                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                ViewBag.Message = "có lỗi!";
                return RedirectToAction("LogIn", "Users");
            }   
        }

        public ActionResult LogOut()
        {
            Session.Clear();
            var fb = new FacebookClient();
            
            HttpCookie cookie = Request.Cookies["user_id"];
            cookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(cookie);

            return RedirectToAction("LogIn", "Users");
        }

        // GET: Users
        public async Task<ActionResult> Index()
        {
            return View(await db.Users.ToListAsync());
        }

        // GET: Users/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,AccountName,Password,Name,Phone,Email,Role")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(user);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(user);
        }

        // GET: Users/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,AccountName,Password,Name,Phone,Email,Role")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            User user = await db.Users.FindAsync(id);
            db.Users.Remove(user);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
