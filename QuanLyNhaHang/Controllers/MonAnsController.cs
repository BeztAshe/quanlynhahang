﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QuanLyNhaHang.Models;

namespace QuanLyNhaHang.Controllers
{
    public class MonAnsController : Controller
    {
        private QuanLyNhaHangContext db = new QuanLyNhaHangContext();

        // GET: MonAns
        public async Task<ActionResult> Index()
        {
            return View(await db.MonAns.ToListAsync());
        }

        // GET: MonAns/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MonAn monAn = await db.MonAns.FindAsync(id);
            if (monAn == null)
            {
                return HttpNotFound();
            }
            return View(monAn);
        }

        // GET: MonAns/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MonAns/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,TenMon,Gia")] MonAn monAn)
        {
            if (ModelState.IsValid)
            {
                db.MonAns.Add(monAn);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(monAn);
        }

        // GET: MonAns/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MonAn monAn = await db.MonAns.FindAsync(id);
            if (monAn == null)
            {
                return HttpNotFound();
            }
            return View(monAn);
        }

        // POST: MonAns/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,TenMon,Gia")] MonAn monAn)
        {
            if (ModelState.IsValid)
            {
                db.Entry(monAn).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(monAn);
        }

        // GET: MonAns/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MonAn monAn = await db.MonAns.FindAsync(id);
            if (monAn == null)
            {
                return HttpNotFound();
            }
            return View(monAn);
        }

        // POST: MonAns/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            MonAn monAn = await db.MonAns.FindAsync(id);
            db.MonAns.Remove(monAn);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult DatMon()
        {
            var currentDay = DateTime.Now.DayOfWeek;
            var Menu = db.ThucDons.FirstOrDefault(x=>x.DayOfWeek == currentDay);
            if(Menu != null)
            {
                var MenuDetail = Menu.ThucDonChiTiets;
                return View(MenuDetail);
            }
            else
            {
                ViewBag.Message = "Hôm nay không có món nào hết";
            }
            return View();
        }

        public JsonResult ListMonChon(int MonAnID, bool isAdd, int sl)
        {
            //khởi tạo object
            List<MonAn> listMonAn = new List<MonAn>();
            //check session đã được tạo hay chưa
            if (TempData.ContainsKey("listMonAn"))
                //session đã đc tạo
                listMonAn = TempData["listMonAn"] as List<MonAn>;
            //lấy ra Món ăn đang được thao tác
            var monAn = db.MonAns.FirstOrDefault(x => x.Id == MonAnID);
           

            if (isAdd)
            {
                for (int i = 0; i < sl; i++)
                {

                    //trường hợp thêm mới món ăn
                    listMonAn.Add(monAn);
                }
            }
            else
            {
                var slma = listMonAn.Where(x => x.Id == MonAnID).Count();

                if (slma == 0)
                {
                    return Json(new { Message = "bạn chưa chọn món ăn" }, JsonRequestBehavior.AllowGet);
                }
                if (slma >=sl)
                {
                    for(int i=0; i < sl; i++)
                    {
                        //trường hợp xóa món ăn
                        listMonAn.Remove(listMonAn.FirstOrDefault(x => x.Id == MonAnID));
                        slma = listMonAn.Count();
                    }
                }
                else
                {
                    return Json(new { Message = "Xóa lớn hơn số lượng hiện có" }, JsonRequestBehavior.AllowGet);
                }
                
            }
            TempData["listMonAn"] = listMonAn;
            return Json(new { Message = "Thành Công" }, JsonRequestBehavior.AllowGet);
        }

        

    }
}
