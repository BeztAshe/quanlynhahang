﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QuanLyNhaHang.Models;
using QuanLyNhaHang.View_Model;

namespace QuanLyNhaHang.Controllers
{
    public class HoaDonsController : Controller
    {
        private QuanLyNhaHangContext db = new QuanLyNhaHangContext();

        // GET: HoaDons
        public async Task<ActionResult> Index()
        {
            return View(await db.HoaDons.ToListAsync());
        }

        // GET: HoaDons/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<HoaDonChiTietVM> hoaDonChiTietVMs = new List<HoaDonChiTietVM>();
            HoaDon hoaDon = await db.HoaDons.FindAsync(id);
            var details = hoaDon.HoaDonChiTiets;
            foreach (var item in details)
            {
                HoaDonChiTietVM hoaDonChiTietVM = new HoaDonChiTietVM();
                hoaDonChiTietVM.Id = item.Id;
                hoaDonChiTietVM.MonAnId = item.MonAnId;
                hoaDonChiTietVM.TenMon = item.MonAn.TenMon;
                hoaDonChiTietVM.DonGia = item.MonAn.DonGia;
                hoaDonChiTietVM.SL = item.SL;
                hoaDonChiTietVM.ThanhTien = item.ThanhTien;
                hoaDonChiTietVMs.Add(hoaDonChiTietVM);
            }

            HoaDonVM hoaDonVM = new HoaDonVM();
            hoaDonVM.MaBan = hoaDon.MaBan;
            hoaDonVM.isPaid = hoaDon.IsPaid;
            hoaDonVM.GioBatDau = hoaDon.GioBatDau;
            hoaDonVM.GioKetThuc = hoaDon.GioKetThuc;
            hoaDonVM.TongTien = hoaDon.TongTien;
            hoaDonVM.HoaDonChiTietVM = hoaDonChiTietVMs;

            return View(hoaDonVM);
        }

        // GET: HoaDons/Create
        public ActionResult Create()
        {
            var lstDishes = TempData["listMonAn"] as List<MonAn>;
            HoaDonVM hoaDonVM = new HoaDonVM();
            List<HoaDonChiTietVM> hoaDonChiTietVMs = new List<HoaDonChiTietVM>();
            for (int i = 0; i < lstDishes.Count(); i++)
            {
                var check = hoaDonChiTietVMs.Where(x => x.MonAnId == lstDishes[i].Id).SingleOrDefault();
                if (check == null)
                {
                    HoaDonChiTietVM hoaDonChiTietVM = new HoaDonChiTietVM();
                    hoaDonChiTietVM.TenMon = lstDishes[i].TenMon;
                    hoaDonChiTietVM.DonGia = lstDishes[i].DonGia;
                    hoaDonChiTietVM.SL = 1;
                    hoaDonChiTietVM.ThanhTien = lstDishes[i].DonGia;
                    hoaDonChiTietVMs.Add(hoaDonChiTietVM);
                }
                else
                {
                    var alreadyhavedish = hoaDonChiTietVMs.Where(x => x.Id == lstDishes[i].Id).FirstOrDefault();
                    alreadyhavedish.SL += 1;
                    alreadyhavedish.ThanhTien += alreadyhavedish.DonGia;
                }
            }

            hoaDonVM.GioBatDau = DateTime.Now;
            hoaDonVM.TongTien = lstDishes.Sum(x => x.DonGia);
            hoaDonVM.HoaDonChiTietVM = hoaDonChiTietVMs;
            TempData.Keep("listMonAn");

            return View(hoaDonVM);
        }

        // POST: HoaDons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,MaBan,TongTien,GioBatDau,IsPaid,HoaDonChiTiets")] HoaDon hoaDon)
        {
            try
            {
                var lstDishes = TempData["listMonAn"] as List<MonAn>;
                List<HoaDonChiTiet> lstdetail = new List<HoaDonChiTiet>();
                foreach(var item in lstDishes)
                {
                    var check = lstdetail.Where(x => x.MonAnId == item.Id).FirstOrDefault();
                    if(check == null)
                    {
                        HoaDonChiTiet detail = new HoaDonChiTiet();
                        detail.HoaDonId = hoaDon.Id;
                        detail.MonAnId = item.Id;
                        detail.SL = 1;
                        detail.ThanhTien = item.DonGia;
                        lstdetail.Add(detail);
                    }
                    else
                    {
                        var alreadyhave = lstdetail.Where(x => x.MonAnId == item.Id).FirstOrDefault();
                        alreadyhave.SL += 1;
                        alreadyhave.ThanhTien += item.DonGia;
                    }
                }

                hoaDon.HoaDonChiTiets = lstdetail;

                if(ModelState.IsValid)
                {
                    db.HoaDons.Add(hoaDon);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }

                
            }
            catch(DataException)
            {
                ModelState.AddModelError("", "tạm thời không thể lưu! thử lại sau");
            }

            return View(hoaDon);
        }

        // GET: HoaDons/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            { 
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HoaDon hoaDon = await db.HoaDons.FindAsync(id);
            var detail = hoaDon.HoaDonChiTiets.ToList();
            HoaDonVM bill = new HoaDonVM();
            List<HoaDonChiTietVM> detailbill = new List<HoaDonChiTietVM>();
            foreach(var item in detail)
            {
                HoaDonChiTietVM detailVM = new HoaDonChiTietVM();
                detailVM.Id = item.Id;
                detailVM.MonAnId = item.MonAnId;
                detailVM.TenMon = item.MonAn.TenMon;
                detailVM.DonGia = item.MonAn.DonGia;
                detailVM.SL = item.SL;
                detailVM.ThanhTien = item.ThanhTien;
                detailbill.Add(detailVM);
            }

            bill.Id = hoaDon.Id;
            bill.GioBatDau = hoaDon.GioBatDau;
            bill.isPaid = hoaDon.IsPaid;
            bill.TongTien = hoaDon.TongTien;
            bill.GioKetThuc = DateTime.Now;
            bill.HoaDonChiTietVM = detailbill;
            bill.MaBan = hoaDon.MaBan;

            if (hoaDon == null)
            {
                return HttpNotFound(); 
            }
            return View(bill);
        }

        // POST: HoaDons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,MaBan,TongTien,GioBatDau,GioKetThuc,IsPaid")] HoaDon hoaDon)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(hoaDon).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }
            catch(DataException)
            {
                ModelState.AddModelError("", "Tạm thời không thể lưu! Thử lại sau");
            }
            
            return View(hoaDon);
        }

        // GET: HoaDons/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HoaDon hoaDon = await db.HoaDons.FindAsync(id);
            if (hoaDon == null)
            {
                return HttpNotFound();
            }
            return View(hoaDon);
        }

        // POST: HoaDons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                HoaDon hoaDon = await db.HoaDons.FindAsync(id);
                db.HoaDons.Remove(hoaDon);
                await db.SaveChangesAsync();
            }
            catch(DataException)
            {
                ModelState.AddModelError("","Tạm thời không thể xóa!");
            }
            
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult ListDaChon()
        {

            return View();

        }
    }
}
