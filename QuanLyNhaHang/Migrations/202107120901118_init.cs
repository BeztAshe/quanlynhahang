namespace QuanLyNhaHang.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HoaDons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaBan = c.String(),
                        TongTien = c.Decimal(nullable: false, precision: 18, scale: 2),
                        GioBatDau = c.DateTime(),
                        GioKetThuc = c.DateTime(),
                        IsPaid = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.HoaDonChiTiets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MonAnId = c.Int(nullable: false),
                        HoaDonId = c.Int(nullable: false),
                        SL = c.Int(nullable: false),
                        ThanhTien = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HoaDons", t => t.HoaDonId, cascadeDelete: true)
                .ForeignKey("dbo.MonAns", t => t.MonAnId, cascadeDelete: true)
                .Index(t => t.MonAnId)
                .Index(t => t.HoaDonId);
            
            CreateTable(
                "dbo.MonAns",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenMon = c.String(),
                        DonGia = c.Decimal(nullable: false, precision: 18, scale: 2),
                        HinhAnh = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ThucDons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DayOfWeek = c.Int(nullable: false),
                        MonAn_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MonAns", t => t.MonAn_Id)
                .Index(t => t.MonAn_Id);
            
            CreateTable(
                "dbo.ThucDonChiTiets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MonAnId = c.Int(nullable: false),
                        ThucDonId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MonAns", t => t.MonAnId, cascadeDelete: true)
                .ForeignKey("dbo.ThucDons", t => t.ThucDonId, cascadeDelete: true)
                .Index(t => t.MonAnId)
                .Index(t => t.ThucDonId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        AccountName = c.String(),
                        Password = c.String(),
                        Name = c.String(),
                        Phone = c.Int(nullable: false),
                        Email = c.String(),
                        Role = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HoaDonChiTiets", "MonAnId", "dbo.MonAns");
            DropForeignKey("dbo.ThucDons", "MonAn_Id", "dbo.MonAns");
            DropForeignKey("dbo.ThucDonChiTiets", "ThucDonId", "dbo.ThucDons");
            DropForeignKey("dbo.ThucDonChiTiets", "MonAnId", "dbo.MonAns");
            DropForeignKey("dbo.HoaDonChiTiets", "HoaDonId", "dbo.HoaDons");
            DropIndex("dbo.ThucDonChiTiets", new[] { "ThucDonId" });
            DropIndex("dbo.ThucDonChiTiets", new[] { "MonAnId" });
            DropIndex("dbo.ThucDons", new[] { "MonAn_Id" });
            DropIndex("dbo.HoaDonChiTiets", new[] { "HoaDonId" });
            DropIndex("dbo.HoaDonChiTiets", new[] { "MonAnId" });
            DropTable("dbo.Users");
            DropTable("dbo.ThucDonChiTiets");
            DropTable("dbo.ThucDons");
            DropTable("dbo.MonAns");
            DropTable("dbo.HoaDonChiTiets");
            DropTable("dbo.HoaDons");
        }
    }
}
