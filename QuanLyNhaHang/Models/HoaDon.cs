﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace QuanLyNhaHang.Models
{
    public class HoaDon
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        public string MaBan { get; set; }
        public ICollection<HoaDonChiTiet> HoaDonChiTiets { get; set; }
        public decimal TongTien { get; set; }
        public DateTime? GioBatDau { get; set; }
        public DateTime? GioKetThuc { get; set; }
        public bool IsPaid { get; set; }
    }
}