﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace QuanLyNhaHang.Models
{
    public class MonAn
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string TenMon { get; set; }
        public decimal DonGia { get; set; }
        public string HinhAnh { get; set; }
        public ICollection<ThucDon> ThucDons { get; set; }
        public ICollection<HoaDonChiTiet> HoaDons { get; set; }
    }
}