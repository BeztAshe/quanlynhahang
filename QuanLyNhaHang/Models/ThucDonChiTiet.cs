﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace QuanLyNhaHang.Models
{
    public class ThucDonChiTiet
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [ForeignKey("MonAn")]
        public int MonAnId { get; set; }
        [ForeignKey("ThucDon")]
        public int ThucDonId { get; set; }
        public ThucDon ThucDon { get; set; }
        public MonAn MonAn { get; set; }
    }
}