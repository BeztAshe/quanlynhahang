﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace QuanLyNhaHang.Models
{
    public class HoaDonChiTiet
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [ForeignKey("MonAn")]
        public int MonAnId { get; set; }
        [ForeignKey("HoaDon")]
        public int HoaDonId { get; set; }
        public int SL { get; set; }
        public decimal ThanhTien { get; set; }
        public HoaDon HoaDon { get; set; }
        public MonAn MonAn { get; set; }
    }
}