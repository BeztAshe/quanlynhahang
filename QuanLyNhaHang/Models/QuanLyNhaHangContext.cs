﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace QuanLyNhaHang.Models
{
    public class QuanLyNhaHangContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx

        public QuanLyNhaHangContext() : base("name=QuanLyNhaHangContext")
        {
        }

        public System.Data.Entity.DbSet<QuanLyNhaHang.Models.User> Users { get; set; }

        public System.Data.Entity.DbSet<QuanLyNhaHang.Models.HoaDon> HoaDons { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

        }

        public System.Data.Entity.DbSet<QuanLyNhaHang.Models.MonAn> MonAns { get; set; }

        public System.Data.Entity.DbSet<QuanLyNhaHang.Models.ThucDon> ThucDons { get; set; }
    }
}
